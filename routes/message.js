'use strict';

var https = require("https");
var util = require("util");
var rp = require('request-promise');
var Logger = require('../logger');
var recipe = require('./recipe');

const LUISURL = "westus.api.cognitive.microsoft.com";
const LUISPredictMask = "/luis/v2.0/apps/%s?subscription-key=%s&q=%s&verbose=%s";
const LUISReplyMask = "/luis/v2.0/apps/%s?subscription-key=%s&q=%s&contextid=%s&verbose=%s";
const LUISVerbose = "true";
const appId = "219feb32-7dd1-4a8a-a929-956a7d8fa1e1";
const appKey = "0eb3362c9cdb4235af879c3228181f3e";


exports.predict = function(req, res) {
    var reqMessage = req.body;
    var resRecipe = "";
    var logger = new Logger();

    logger.info('Adding goods: ' + reqMessage);

    var LUISOptions = {
        hostname: LUISURL,
        path: util.format(LUISPredictMask, appId, appKey, encodeURIComponent(reqMessage), LUISVerbose)
      }; 

    httpHelper(LUISOptions, {

        //On success of prediction
        onSuccess: function (response) {
        console.log("Query: " + response.query);
            for (var i = 1; i <= response.entities.length; i++) {
                if(response.entities[i-1].type == "goods"){
                    let dataEntity =response.entities[i-1].entity;

                   recipe.findRecipe(dataEntity, {
                        success: function(response){
                            res.send(200,response);
                        },

                        fail: function(err){
                             res.send(200,err);
                        }
                   });
                }
            }
            if (typeof response.dialog !== "undefined" && response.dialog !== null) {
                console.log("Dialog Status: " + response.dialog.status);
                if(!response.dialog.isFinished()) {
                console.log("Dialog Parameter Name: " + response.dialog.parameterName);
                }
            }
        },

        //On failure of prediction
        onFailure: function (err) {
            console.error(err);
            res.send(err); 
        }
    }) ;
    
    
};


var httpHelper = function (LUISOptions, responseHandlers) {
  var req = https.request(LUISOptions, function (response) {
    var JsonResponse = "";
    console.log("called httpHelper");
    response.on("data", function (chunk) {
      JsonResponse += chunk;
      console.log(JsonResponse);
    });
    response.on("end", function () {
      var LUISresponse = LUISResponse(JsonResponse);
      responseHandlers.onSuccess(LUISresponse);
    });
  });
  req.end();
  req.on("error", function (err) {
    responseHandlers.onFailure(err);
  });
};

var LUISResponse = function (JsonResponse) {
  if (typeof JsonResponse === "undefined" || JsonResponse === null || JsonResponse.length === 0) {
    throw new Error("Invalid Application Id");
  }
  var LUISresponse = JSON.parse(JsonResponse);
  if (LUISresponse.hasOwnProperty("statusCode")) {
    throw new Error("Invalid Subscription Key");
  }
  if (LUISresponse.hasOwnProperty("dialog") && typeof LUISresponse.dialog !== "undefined") {
    LUISresponse.dialog.isFinished = function () {
      return this.status === "Finished";
    };
  }
  return LUISresponse;
};