'use strict';

var https = require("https");
var util = require("util");
var rp = require('request-promise');
var Logger = require('../logger');

//LUIS Setting
const LUISURL = "westus.api.cognitive.microsoft.com";
const LUISPredictMask = "/luis/v2.0/apps/%s?subscription-key=%s&q=%s&verbose=%s";
const LUISReplyMask = "/luis/v2.0/apps/%s?subscription-key=%s&q=%s&contextid=%s&verbose=%s";
const LUISVerbose = "true";
const appId = "219feb32-7dd1-4a8a-a929-956a7d8fa1e1";
const appKey = "0eb3362c9cdb4235af879c3228181f3e";

exports.predict = function(req, res) {
  
    var logger = new Logger();

    logger.info('kakao conversation start. ');
    logger.info('usesr input message : ' + req.body.content);
    var reqMessage = req.body.content;
        
    logger.info('LUIS Process Start.' );
    var LUISOptions = {
        hostname: LUISURL,
        path: util.format(LUISPredictMask, appId, appKey, encodeURIComponent(reqMessage), LUISVerbose)
      }; 
    
    httpHelper(LUISOptions, {
        
        //On success of prediction
        onSuccess: function (response) {
        logger.info('LUIS Process : onSuccess start.' );
        console.log("Query: " + response.query);
            for (var i = 1; i <= response.entities.length; i++) {
                if(response.entities[i-1].type == "goods"){
                    let dataEntity =response.entities[i-1].entity;

                   recipe.findRecipe(dataEntity, {
                        success: function(response){
                            logger.info('LUIS Result : Success!' );
                            res.send(200,response);
                        },

                        fail: function(err){
                             logger.error('LUIS Result : Error!' );
                             logger.error('Error : ' +  err);
                             res.send(200,err);
                        }
                   });
                }
            }
            if (typeof response.dialog !== "undefined" && response.dialog !== null) {
                console.log("Dialog Status: " + response.dialog.status);
                if(!response.dialog.isFinished()) {
                console.log("Dialog Parameter Name: " + response.dialog.parameterName);
                }
            }
            
        },

        //On failure of prediction
        onFailure: function (err) {
            logger.info('LUIS Process : onFailure' );
            logger.error('LUIS Result : Error!' );
            logger.error('Error : ' +  err);
            console.error(err);
            res.send(err); 
        }
    }) ;
    
    logger.info('LUIS Response : ' + res );
    console.log(res);
    logger.info('LUIS Process End.' );
    return res.json({
            "message" : {
            "text" : "your order complete!!"
            }
    })
};


var httpHelper = function (LUISOptions, responseHandlers) {
  var req = https.request(LUISOptions, function (response) {
    var JsonResponse = "";
    logger.info('LUIS Process : httpHelper' );
    console.log("called httpHelper");
    response.on("data", function (chunk) {
      JsonResponse += chunk;
      logger.info('JsonResponse : ' + JsonResponse );
      console.log(JsonResponse);
    });
    response.on("end", function () {
      var LUISresponse = LUISResponse(JsonResponse);
      responseHandlers.onSuccess(LUISresponse);
    });
  });
  req.end();
  req.on("error", function (err) {
    responseHandlers.onFailure(err);
  });
};

var LUISResponse = function (JsonResponse) {
  if (typeof JsonResponse === "undefined" || JsonResponse === null || JsonResponse.length === 0) {
    logger.error('LUIS Result : Invalid Application Id' );
    throw new Error("Invalid Application Id");
  }
  var LUISresponse = JSON.parse(JsonResponse);
  if (LUISresponse.hasOwnProperty("statusCode")) {
    logger.error('LUIS Result : Invalid Subscription Key' );
    throw new Error("Invalid Subscription Key");
  }
  if (LUISresponse.hasOwnProperty("dialog") && typeof LUISresponse.dialog !== "undefined") {
    LUISresponse.dialog.isFinished = function () {
      return this.status === "Finished";
    };
  }
  return LUISresponse;
};