var winston = require('winston');
var winstonDailyRotateFile = require('winston-daily-rotate-file');
var fs = require('fs');
var logDir = 'logs';
var tsFormat = () => (new Date()).toLocaleTimeString();

if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}

module.exports = function(){

    var logger = new (winston.Logger)({
        transports: [
            new (winston.transports.Console)({
            name:'consoleLog',
            colorize:false,
            timestamp: tsFormat,
            json:false
            }),
            new (winstonDailyRotateFile)({
            name: 'debugLog',
            level:'debug',
            filename: './logs/debug_',
            maxsize:1000000,
            maxFiles:1,
            timestamp: tsFormat,
            datePattern : 'yyyyMMdd.log',
            json:false
            }),
            new (winstonDailyRotateFile)({
            name: 'infoLog',
            level:'info',
            filename: './logs/info_',
            maxsize:1000000,
            maxFiles:1,
            timestamp: tsFormat,
            datePattern : 'yyyyMMdd.log',
            json:false
            }),
            new (winstonDailyRotateFile)({
            name: 'errorLog',
            level:'error',
            filename: './logs/error_',
            maxsize:1000000,
            maxFiles:1, 
            timestamp: tsFormat,
            datePattern : 'yyyyMMdd.log',
            json:false
            })          
        ]
    });

return logger;
};