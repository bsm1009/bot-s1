var path = require('path')
var webpack = require('webpack')

module.exports = function (env) {
  return {
    entry: {
            main: './app_kakao.js'
           // vendor: 'moment',
            //common:'./logger.js'
        },
      output: {
            filename: '[name].js',
            path: path.resolve(__dirname, 'dist')
        },
      module: {
       rules: [
      {
      test: /\.js$/,
      exclude: /(node_modules|bower_components)/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: ['env']
        }
      }
    }
    ]
      },

      plugins: [
        new webpack.optimize.UglifyJsPlugin({
       // sourceMap: options.devtool && (options.devtool.indexOf("sourcemap") >= 0 || options.devtool.indexOf("source-map") >= 0),
          compressor: {
            warnings: true,
          },
        }),
        
      //  new webpack.optimize.CommonsChunkPlugin({
      //           name: 'vendor',
      //           minChunks: function (module) {
      //              // this assumes your vendor imports exist in the node_modules directory
      //              return module.context && module.context.indexOf('node_modules') !== -1;
      //           }
      //       }),
      //       //CommonChunksPlugin will now extract all the common modules from vendor and main bundles
      //       new webpack.optimize.CommonsChunkPlugin({ 
      //           name: 'manifest' //But since there are no more common modules between them we end up with just the runtime code included in the manifest file
      //       }),
      new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    })
        
       ],
      
      // resolve: {
      //   mainFields: ['main', 'web']
      //  },

      node: {    
        fs: 'empty',
        net: 'empty',
        tls: 'empty',
        dns: 'empty'
      }
  }
}