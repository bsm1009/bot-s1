'use strict';
require('dotenv').load();

var restify = require('restify');
var rp = require('request-promise');
var message = require('./routes/message_kakao');
var Logger = require('./logger');
var logger = new Logger();

// Restify Server 셋팅
var app = restify.createServer();
app.use(restify.CORS()); 
app.use(restify.bodyParser()); 

app.listen(80, function () {
   console.log('%s listening to %s', app.name, app.url); 
   logger.debug('%s debug listening to %s', app.name, app.url); 
   logger.info('%s info listening to %s', app.name, app.url); 
});

app.get('/keyboard', (req, res) => {
    return res.json({
        "type" : "text"
    })
});

app.post('/message', message.predict);
